import { assets } from '../../assets'
import './sidebar.scss';
const Sidebar = () => {
  return (
    <div className='sidebar'>
      <div className="top">
        <img src={assets.menu_icon} alt="Menu" className='menu' />
        <div className="new-chat">
          <img src={assets.plus_icon} alt="Add new chat" />
          <p>New Chat</p>
        </div>
        <div className="recent">
          <p className="recent-title">Recent</p>
          <div className="recent-entry">
            <img src={assets.message_icon} alt="Message" />
            <p>What is react...</p>
          </div>
        </div>
      </div>
      <div className="bottom">
        <div className="bottom-item recent-entry">
          <img src={assets.question_icon} alt="Help" />
          <p>Help</p>
        </div>
        <div className="bottom-item recent-entry">
          <img src={assets.history_icon} alt="Activity" />
          <p>Activity</p>
        </div>
        <div className="bottom-item recent-entry">
          <img src={assets.setting_icon} alt="Settings" />
          <p>Settings</p>
        </div>
      </div>
    </div>
  )
}

export default Sidebar