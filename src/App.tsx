import './index.scss'
import Sidebar from './components/sidebar/sidebar'

function App() {

  return (
    <>
      <Sidebar />
    </>
  )
}

export default App
